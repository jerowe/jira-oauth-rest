package JIRA::OAuth::REST;

use strict;
use 5.008_005;
our $VERSION = '0.01';

1;
__END__

=encoding utf-8

=head1 NAME

JIRA::OAuth::REST - Blah blah blah

=head1 SYNOPSIS

  use JIRA::OAuth::REST;

=head1 DESCRIPTION

JIRA::OAuth::REST is

=head1 AUTHOR

Jillian Rowe E<lt>jillian.e.rowe@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2016- Jillian Rowe

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

=cut
