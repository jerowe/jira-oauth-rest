# NAME

JIRA::OAuth::REST - Blah blah blah

# SYNOPSIS

    use JIRA::OAuth::REST;

# DESCRIPTION

JIRA::OAuth::REST is

# AUTHOR

Jillian Rowe &lt;jillian.e.rowe@gmail.com>

# COPYRIGHT

Copyright 2016- Jillian Rowe

# LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# SEE ALSO
